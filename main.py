import numpy as np
import time

a = np.random.rand(1_000_000)
b = np.random.rand(1_000_000)

def vectorized():
    tic = time.time()
    c = np.dot(a,b)
    toc = time.time()

    print("Vectorized version:" + str(1000*(toc-tic)) + "ms")

def for_loop():
    tic = time.time()
    c = 0
    
    for i in range(1_000_000):
        c += a[i]*b[i]
    
    toc = time.time()
    
    print("For loop: " + str(1000*(toc-tic)) + "ms")

vectorized()
for_loop()